#!/bin/sh
#
# isdown-helper.sh
# author: klosure
# https://gitlab.com/klosure/isdown
# license: bsd 3-clause
#
# a helper script for isdown.sh
# (not to be called on its own)

BASE_URL="https://isdown.me/"


# were we called by cbr-convert?
fun_check_caller () {
    if ! [ "$1" = "secretargument" ] ; then
	echo  "Do not call this script on its own, use isdown!"
    elif [ "$#" -lt 2 ] ; then
	echo "You must pass a site"
    fi
}


# this function just helps us strip off any leftover http or https prefixes
# http://google.com becomes google.com
fun_check_url_format () {
    L_URL="$1"
    L_FIRST_SEVEN=$(echo "$L_URL" | cut -c-7)
    L_FIRST_EIGHT=$(echo "$L_URL" | cut -c-8)
    if [ "$L_FIRST_SEVEN" = "http://"  ] ; then
	L_URL=$(echo "$L_URL" | cut -c8-)
    elif [ "$L_FIRST_EIGHT" = "https://" ] ; then
	L_URL=$(echo "$L_URL" | cut -c9-)
    fi
    echo "$L_URL"
    unset L_URL L_FIRST_SEVEN L_FIRST_EIGHT
}


# lets check the provided site!
fun_check_site () {
    L_SITE=$(fun_check_url_format "$1")
    L_BUILT_URL="$BASE_URL""$L_SITE" # creates something like https://isdown.me/mysite.com
    L_REZ=$(curl --silent "$L_BUILT_URL" | grep "<title>")
    L_REZ_TRIM=$(echo "$L_REZ" | cut -d">" -f2 | cut -d":" -f1)
    case "$L_REZ_TRIM" in
	Yay,*) echo "$L_REZ_TRIM";;
	Oh*)   echo "$L_REZ_TRIM";;
	*)     echo "An error occurred testing the site. Usually this means you passed an invalid site to isdown. (devs, debug in fun_check_site())";;
    esac
    unset L_SITE L_BUILT_URL L_REZ L_REZ_TRIM
}


# --- ENTRY POINT ---
fun_check_caller "$@"
fun_check_site "$2"
