# isdown

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

Do you need to quickly check a site to see if it's down? Do you need to check a whole list of sites? Then this will likely be a helpful tool for you. Run a one liner from the command line and you're off! You can run multipile checks in parallel, allowing more free time for all the fun things in life; like writing more shell scripts! 👍


I've tried to make this tool as intuitive as possible, with sane defaults, and no weird flags. We use the web service [isdown.me](https://isdown.me/) behind the scenes to do each check. 

### Dependencies
In order to use this tool, you only need a few things installed:

+ POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html)
+ POSIX [shell](https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html)
+ [Curl](https://curl.haxx.se/)
+ [GNU Parallel](https://www.gnu.org/software/parallel)

### Installation
Simply run:

```bash
make install      #(as root)

# to uninstall
make uninstall    #(as root)
```

> Note: If you choose to install this by hand, be sure the helper file is also in the [$PATH](https://kb.iu.edu/d/acar)

### Examples
```bash
# test a single site
$ isdown openbsd.org
Yay, openbsd.org is up.

# read in sites from a text file and test one at a time
$ isdown -f ./sites.txt
Yay, fsf.org is up.
Oh No! gentoo.org is down.
Yay, openbsd.org is up.
Yay, netbsd.org is up.
(cont......)

# test 3 sites; using up to 2 threads
$ isdown -j 2 fsf.org gentoo.org openbsd.org
Yay, fsf.org is up.
Yay, gentoo.org is up.
Yay, openbsd.org is up.

# read in sites from 2 files; using a maximum of 8 threads
$ isdown -j 8 -f ./sitesA.txt ./sitesB.txt
Yay, A1.org is up.
Yay, B1.org is up.
Oh No! B2.org is down.
Yay, A2.xyz is up.
Yay, B3.org is up.
Yay, B4.org is up.
Yay, B5.org is up.
Yay, B6.xyz is up.
(cont............)
```

### Notes
When reading from a file, `isdown.sh` expects them to be newline delimited. Sites will be stripped of any http(s) prefixes. By default (*i.e.* without the `-j` flag) the number of jobs kicked off is half of the available system cores. 

### License / Disclaimer
This project is licensed under the 3-clause BSD license. (See LICENSE.md)


This project is not related to *isdown.me* and their organization.
