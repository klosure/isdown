#!/bin/sh
#
# isdown.sh
# author: klosure
# https://gitlab.com/klosure/isdown
# license: bsd 3-clause
#
# A shell script to test if a web host is alive.
# Uses the web service https://isdown.me
# 
# Dependencies: GNU Parallel, curl


VERSION="0.2.2"

# Portable way to get the number of cores available to the system (Linux, BSD, and MacOS)
CORES_AVAIL=$(getconf _NPROCESSORS_ONLN 2>/dev/null || sysctl -n hw.ncpu || echo 2)
# Default number of threads to use (half available cores)
JOBS=$(( CORES_AVAIL / 2 ))


# read sites from a newline separated text file
fun_proc_files () {
    L_FIRST_TWO=$(echo "$@" | grep -o "^..")
    case "$L_FIRST_TWO" in
	-f) shift 1;;
	# -j is present
	-j)  shift 3;;
    esac
    
    L_SITES=""
    # yes, we do want to split
    # shellcheck disable=SC2068
    for FILE in $@ ; do
	while read -r SITE ; do
	    L_SITES="$L_SITES $SITE"
	done < "$FILE"
    done
    fun_run "$L_SITES"
    unset L_FIRST_TWO L_SITES
}


# echoes out all the websites passed into it
fun_echo_files () {
    # yes, we do want to split
    # shellcheck disable=SC2068
    for SITE in $@ ; do
	echo "$SITE"
    done
}


# call this function to start the script
fun_run () {
    fun_echo_files "$@" | parallel -j $JOBS isdown-helper "secretargument" {}  
}


# run when ctrl-c is pressed
fun_cleanup () {
    # doesn't need to do anything yet
    echo " isdown caught sigint, exiting.."
    exit 0
}


# print version info
fun_print_ver () {
    echo "version: $VERSION"
}


# print usage info
fun_print_usage () {
    echo "-- usage: ------------------------------------------"
    echo "       isdown openbsd.org                            # test a single site"
    echo "       isdown -f ./sites.txt                         # read in sites from a text file and test one at a time"
    echo "       isdown -j 2 openbsd.org slashdot.org fsf.org  # test 3 sites, using 2 threads"
    echo "       isdown -h                                     # shows this help message"
    echo "       isdown -v                                     # show the version info"
    echo ""
    echo "       (by default the number of jobs started is half the available system cores)"
}

# print help/version info
fun_print_info () {
    echo "isdown - check if a website is alive and responsive"
    fun_print_ver
    echo "license: bsd 3-clause"
    echo "author: klosure"
    echo "site: https://gitlab.com/klosure/isdown"
    fun_print_usage
    exit 0
}


# print error message
fun_error () {
    echo "$1"
    fun_print_usage
    exit 1
}


# make sure that our dependencies are installed and executable
fun_check_deps () {
    if ! [ -x "$(command -v curl)" ]; then
	fun_error "error: isdown needs curl, please install it."
    elif ! [ -x "$(command -v parallel)" ]; then
	fun_error "error: isdown needs GNU parallel, please install it."
    fi
}

# --- ENTRY POINT ---
trap fun_cleanup INT # catch ctrl-c
fun_check_deps

# check for flags passed in by the user
while getopts 'j:f:hv' OPT ; do
    case "$OPT" in
	j)      JOBS="$OPTARG";;
	f)	fun_proc_files "$@" && exit 0;;
	h)      fun_print_info;;
	v)      fun_print_ver && exit 0;;
	[?])	fun_error "error: please use one of these flags!";;
	esac
done

# check if we need to strip off the -j flag after setting it
GRAB_TWO=$(echo "$@" | grep -o "^..")
case "$GRAB_TWO" in
    -j)  shift 2;;
esac

fun_run "$@"

exit 0
