PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man/man1

.POSIX: install

all: help

help:
	@echo "please run 'make install' as root"
install:
	cp ./isdown.sh $(BINDIR)/isdown
	cp ./isdown-helper.sh $(BINDIR)/isdown-helper
	cp ./isdown.1 $(MANDIR)
uninstall:
	rm $(BINDIR)/isdown
	rm $(BINDIR)/isdown-helper
	rm $(MANDIR)/isdown.1
test:
	shellcheck isdown-helper.sh isdown.sh
